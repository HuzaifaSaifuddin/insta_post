require "rails_helper"

RSpec.feature "Navbar", type: :feature do
  load Rails.root.to_s + "/spec/helpers/sessions_helper.rb"

  before :each do
    visit "/users/sign_in"
    @user = (User.all[-1] if User.all[-1].present?) || FactoryBot.create(:user)
    sign_in(@user)
  end

  feature "Navbar Actions" do
    scenario "'HOME' Link", js: true do
      visit "/"
      click_on "Home"
      sleep 2
      expect(current_path).to eq("/posts")
    end

    scenario "'PROFILE' Link", js: true do
      visit "/"
      click_on "Profile"
      sleep 2
      expect(current_path).to eq("/profile_settings")
    end

    scenario "Search Field", js: true do
      visit "/"
      fill_in "search-users", with: @user.first_name
      find(:css, "a#ui-id-2").click
      sleep 2
      expect(current_path).to eq("/profile_settings")
    end

    scenario "Logout", js: true do
      visit "/"
      click_on "Logout"
      sleep 2
      expect(current_path).to eq("/users/sign_in")
    end
  end
end
