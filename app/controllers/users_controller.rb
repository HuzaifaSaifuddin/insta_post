class UsersController < ApplicationController
  before_action :authorize

  def search
    @users = User.where("full_name LIKE ?", "%#{params[:q]}%").limit(10)
  end
end
