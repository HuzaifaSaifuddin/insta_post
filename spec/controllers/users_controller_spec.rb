require "rails_helper"

RSpec.describe UsersController, type: :controller do
  user = FactoryBot.build_stubbed(:user)
  before { allow(controller).to receive(:current_user) { user } }

  describe "#search" do
    it "renders search.json.jbuilder" do
      get :search, params: { q: "Huza" }, format: :json
      expect(subject).to render_template(:search)
    end
  end
end
