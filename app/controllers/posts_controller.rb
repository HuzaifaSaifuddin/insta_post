class PostsController < ApplicationController
  before_action :authorize
  before_action :find_post, only: %i[edit update destroy]

  def index
    show_posts_of = [current_user.id] + current_user.following.pluck(:id)
    @posts = Post.includes(:user).where(user_id: show_posts_of).order(created_at: :desc)

    @post = Post.new
  end

  def new; end

  def create
    @post = Post.new(post_params)

    head :ok unless @post.save
  end

  def edit; end

  def update
    head :ok unless @post.update(post_params)
  end

  def destroy
    @post.destroy if @post.present?
  end

  private

  def post_params
    params.require(:post).permit(:content, :user_id, post_attachments_attributes: [:id, :post_id, :avatar])
  end

  def find_post
    @post = Post.find_by(id: params[:id])
  end
end
