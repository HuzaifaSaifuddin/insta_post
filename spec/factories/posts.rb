FactoryBot.define do
  factory :post do
    content { Faker::Name.name }
    user_id { FactoryBot.create(:user, email: Faker::Internet.email).id }
  end
end
