Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "posts#index"

  resources :posts

  resources :profile_settings, only: [:index] do
    collection do
      get :followers
      get :followings
    end
  end

  resources :relationships, only: %i[create destroy]

  get "users/search"
  devise_for :users, controllers: { registrations: "registrations" }
end
