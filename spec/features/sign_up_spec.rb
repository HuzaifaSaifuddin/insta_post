require "rails_helper"

RSpec.feature "SignUp", type: :feature do
  load Rails.root.to_s + "/spec/helpers/sessions_helper.rb"

  feature "SignUp Form" do
    scenario "User can Signup when form is filled", js: true do
      visit "/users/sign_up"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Sign up")
      sign_up
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Home")
      expect(page).to have_content("Profile")
      expect(page).to have_content("Logout")
    end

    scenario "User cant Signup when form is not filled", js: true do
      visit "/users/sign_up"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Sign up")
      click_on("Sign up")
      sleep 2
      expect(page).to have_content("First name can't be blank")
      expect(page).to have_content("Last name can't be blank")
      expect(page).to have_content("Email can't be blank")
      expect(page).to have_content("Password can't be blank")
    end

    scenario "User cant Signup when password and confirmation password dont match", js: true do
      visit "/users/sign_up"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Sign up")
      fill_in "user[first_name]", with: Faker::Name.first_name
      fill_in "user[last_name]", with: Faker::Name.last_name
      fill_in "user[email]", with: Faker::Internet.email
      fill_in "user[password]", with: "Test@123"
      fill_in "user[password_confirmation]", with: "Test123"
      click_on("Sign up")
      sleep 2
      expect(page).to have_content("Password confirmation doesn't match Password")
    end

    scenario "redirects to signin page on clicking Sign in Link", js: true do
      visit "/users/sign_up"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Sign up")
      click_on("Log in")
      sleep 2
      expect(current_path).to eq("/users/sign_in")
    end
  end
end
