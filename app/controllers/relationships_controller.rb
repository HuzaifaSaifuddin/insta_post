class RelationshipsController < ApplicationController
  before_action :authorize

  def create
    @user = User.find_by(id: params[:followed_id])
    @relationship = current_user.active_relationships.find_by(followed_id: params[:followed_id])
    current_user.follow(@user) if @user.present? && !@relationship.present?
  end

  def destroy
    @user = User.find_by(id: params[:id])
    current_user.unfollow(@user) if @user.present?
  end
end
