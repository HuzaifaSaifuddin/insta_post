FactoryBot.define do
  factory :user do
    first_name = Faker::Name.first_name
    last_name = Faker::Name.last_name

    full_name { "#{first_name} #{last_name}" }
    first_name { first_name }
    last_name { last_name }
    email { Faker::Internet.email }
    password { 'Test@123' }
  end
end
