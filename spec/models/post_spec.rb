require "rails_helper"

RSpec.describe Post, type: :model do
  it "has a valid factory" do
    expect(FactoryBot.build_stubbed(:post)).to be_valid
  end

  it "responds to fields" do
    should respond_to(:content)
    should respond_to(:avatar)
    should respond_to(:user)
  end

  # Validation
  it { is_expected.to validate_presence_of(:content) }
  it { is_expected.to belong_to(:user) }

  # Relations
  it { expect(Post.reflect_on_association(:user).macro).to eq(:belongs_to) }
end
