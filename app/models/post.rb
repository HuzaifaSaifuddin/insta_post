class Post < ApplicationRecord
  belongs_to :user
  has_many :post_attachments

  validates_presence_of :content

  accepts_nested_attributes_for :post_attachments
end
