def sign_up
  fill_in "user[first_name]", with: Faker::Name.first_name
  fill_in "user[last_name]", with: Faker::Name.last_name
  fill_in "user[email]", with: Faker::Internet.email
  fill_in "user[password]", with: "Test@123"
  fill_in "user[password_confirmation]", with: "Test@123"
  click_on("Sign up")
end

def sign_in(user)
  fill_in "user[email]", with: user.email
  fill_in "user[password]", with: "Test@123"
  click_on("Log in")
end
