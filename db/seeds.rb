# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

100.times do |i|
  puts i
  User.create([{ first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email, password: "Test@123" }])
end

1000.times do |i|
  puts i
  user1 = User.all[Faker::Number.number(2).to_i]
  user2 = User.all[Faker::Number.number(2).to_i]
  user1.follow(user2) unless user1.following?(user2)
end

User.all.each do |user|
  user.posts.create([{ content: "#{user.full_name}'s First Post", avatar: File.open(File.join(Rails.root, "app/assets/images/insta-post.png")) }])
end
