require "rails_helper"

RSpec.describe ProfileSettingsController, type: :controller do
  user = FactoryBot.build_stubbed(:user)
  before { allow(controller).to receive(:current_user) { user } }

  describe "#index" do
    it "renders index.html.erb" do
      get :index, params: { user_id: FactoryBot.create(:user).id }
      expect(subject).to render_template(:index)
    end
  end

  describe "#followers" do
    it "renders followers.js.erb" do
      get :followers, params: { user_id: FactoryBot.create(:user).id }
      expect(subject).to render_template(:followers)
    end
  end

  describe "#followings" do
    it "renders followings.js.erb" do
      get :followings, params: { user_id: FactoryBot.create(:user).id }
      expect(subject).to render_template(:followings)
    end
  end
end
