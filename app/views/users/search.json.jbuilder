json.array!(@users) do |user|
  json.id user.id.to_s
  json.label user.full_name
  json.value user.full_name
end
