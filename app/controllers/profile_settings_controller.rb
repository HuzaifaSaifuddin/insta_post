class ProfileSettingsController < ApplicationController
  before_action :authorize

  def index
    @user = User.find_by(id: params[:user_id])
    @posts = @user.posts

    @post = Post.new
  end

  def followers
    @user = User.find_by(id: params[:user_id])
    @followers_list = @user.followers
  end

  def followings
    @user = User.find_by(id: params[:user_id])
    @followings_list = @user.following
  end
end
