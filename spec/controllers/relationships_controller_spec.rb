require "rails_helper"

RSpec.describe RelationshipsController, type: :controller do
  user = FactoryBot.create(:user, email: Faker::Internet.email)
  before { allow(controller).to receive(:current_user) { user } }

  describe "#create" do
    it "renders create.html.erb" do
      post :create, params: { followed_id: FactoryBot.create(:user).id }, format: :js, xhr: true
      expect(subject).to render_template(:create)
    end
  end

  describe "#destroy" do
    it "renders destroy.html.erb" do
      delete :destroy, params: { id: FactoryBot.create(:user).id }, format: :js, xhr: true
      expect(subject).to render_template(:destroy)
    end
  end
end
