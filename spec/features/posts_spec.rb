require "rails_helper"

RSpec.feature "Posts Homepage", type: :feature do
  load Rails.root.to_s + "/spec/helpers/sessions_helper.rb"

  before :each do
    visit "/users/sign_in"
    @user = User.all[-1] if User.all[-1].present? || FactoryBot.create(:user)
    sign_in(@user)

    @content = Faker::Name.name
    @content1 = Faker::Name.name
  end

  feature "Posts CRUD" do
    scenario "User can create post & view it", js: true do
      visit "/posts"
      fill_in "post[content]", with: @content
      click_on "Click to Post"
      sleep 2
      expect(page).to have_content(@content)
    end

    scenario "User can edit/update post", js: true do
      visit "/posts"
      fill_in "post[content]", with: @content
      click_on "Click to Post"
      sleep 2
      expect(page).to have_content(@content)
      @post = @user.posts[-1]
      find(:css, "a.edit_post#{@post.id}").click
      sleep 2
      within "#edit_post_#{@post.id}" do
        expect(find_field("post[content]").value).to eq @post.content
        click_on "Close"
      end
      expect(page).to have_content(@content)
      find(:css, "a.edit_post#{@post.id}").click
      sleep 2
      within "#edit_post_#{@post.id}" do
        expect(find_field("post[content]").value).to eq @post.content
        fill_in "post[content]", with: @content1
        click_on "Update"
      end
      expect(page).to have_content(@content1)
    end

    scenario "User can delete post", js: true do
      visit "/posts"
      fill_in "post[content]", with: @content1
      click_on "Click to Post"
      sleep 2
      expect(page).to have_content(@content1)
      @post = @user.posts[-1]
      find(:css, "a.delete_post#{@post.id}").click
      sleep 2
      expect(page).to have_no_content(@content1)
    end
  end
end
