require "rails_helper"

RSpec.feature "SignIn", type: :feature do
  load Rails.root.to_s + "/spec/helpers/sessions_helper.rb"

  before :each do
    @user = FactoryBot.create(:user)
  end

  feature "SignIn Form" do
    scenario "User can Signin when username/password are filled and match", js: true do
      visit "/users/sign_in"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Log in")
      sign_in(@user)
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Home")
      expect(page).to have_content("Profile")
      expect(page).to have_content("Logout")
    end

    scenario "User cant Signin when username/password are not filled", js: true do
      visit "/users/sign_in"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Log in")
      click_on("Log in")
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_no_content("Home")
      expect(page).to have_no_content("Profile")
      expect(page).to have_no_content("Logout")
    end

    scenario "User cant Signin when username/password are filled and dont match", js: true do
      visit "/users/sign_in"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Log in")
      fill_in "user[email]", with: @user.email
      fill_in "user[password]", with: "Test@1234"
      click_on("Log in")
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_no_content("Home")
      expect(page).to have_no_content("Profile")
      expect(page).to have_no_content("Logout")
    end

    scenario "redirects to signin page on clicking Sign up Link", js: true do
      visit "/users/sign_in"
      sleep 2
      expect(page).to have_content("InstaPost")
      expect(page).to have_content("Log in")
      click_on("Sign up")
      sleep 2
      expect(current_path).to eq("/users/sign_up")
    end
  end
end
