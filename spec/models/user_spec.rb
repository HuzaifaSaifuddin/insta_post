require "rails_helper"

RSpec.describe User, type: :model do
  it "has a valid factory" do
    expect(FactoryBot.build_stubbed(:user)).to be_valid
  end

  it "responds to fields" do
    should respond_to(:full_name)
    should respond_to(:first_name)
    should respond_to(:last_name)
    should respond_to(:email)
    should respond_to(:password)
  end

  # Validation
  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to validate_presence_of(:last_name) }
  it { is_expected.to validate_presence_of(:email) }

  # Relations
  it { expect(User.reflect_on_association(:posts).macro).to eq(:has_many) }
  it { expect(User.reflect_on_association(:active_relationships).macro).to eq(:has_many) }
  it { expect(User.reflect_on_association(:passive_relationships).macro).to eq(:has_many) }

  # set_full_name
  it "sets full_name" do
    user = FactoryBot.build_stubbed(:user)
    user.set_full_name
    expect(user.set_full_name).to eq("#{user.first_name} #{user.last_name}")
  end

  # follow / following?
  it "follow users" do
    user = FactoryBot.create(:user)
    user1 = FactoryBot.create(:user, email: Faker::Internet.email)
    user.follow(user1)

    expect(user.following.count).to eq(1)
    expect(user.following?(user1)).to be true
  end

  # unfollow / following?
  it "unfollow users" do
    user = FactoryBot.create(:user)
    user1 = FactoryBot.create(:user, email: Faker::Internet.email)
    user.unfollow(user1)

    expect(user.following.count).to eq(0)
    expect(user.following?(user1)).to be false
  end
end
