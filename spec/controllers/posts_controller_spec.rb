require "rails_helper"

RSpec.describe PostsController, type: :controller do
  user = FactoryBot.build_stubbed(:user)
  before { allow(controller).to receive(:current_user) { user } }

  describe "#index" do
    it "renders index.html.erb" do
      get :index
      expect(subject).to render_template(:index)
    end
  end

  describe "#new" do
    it "renders new.js.erb" do
      get :new, format: :js, xhr: true
      expect(subject).to render_template(:new)
    end
  end

  describe "#create" do
    it "creates a new post" do
      expect do
        post :create, params: { post: FactoryBot.attributes_for(:post) }, format: :js, xhr: true
      end.to change(Post, :count).by(1)
    end

    it "renders create.js.erb on success" do
      post :create, params: { post: FactoryBot.attributes_for(:post) }, format: :js, xhr: true
      expect(subject).to render_template(:create)
    end

    it "renders nothing on failure" do
      post :create, params: { post: FactoryBot.attributes_for(:post, content: nil) }, format: :js, xhr: true
      expect(subject).to render_template(nil)
    end
  end

  describe "#edit" do
    it "renders edit.js.erb" do
      get :edit, params: { id: FactoryBot.create(:post).id }, format: :js, xhr: true
      expect(subject).to render_template(:edit)
    end
  end

  describe "#update" do
    post = FactoryBot.create(:post, content: "Some Content")
    it "renders update.js.erb on success" do
      patch :update, params: { id: post.id, post: post.attributes }, format: :js, xhr: true
      expect(subject).to render_template(:update)
    end

    it "renders nothing on failure" do
      patch :update, params: { id: post.id, post: { content: nil } }, format: :js, xhr: true
      expect(subject).to render_template(nil)
    end

    it "changes post's attributes" do
      patch :update, params: { id: post.id, post: post.attributes }, format: :js, xhr: true
      post.reload
      expect(post.content).to eq("Some Content")
    end
  end

  describe "#destroy" do
    post = FactoryBot.create(:post)
    it "renders close.js.erb on success" do
      delete :destroy, params: { id: post.id }, format: :js, xhr: true
      expect(subject).to render_template(:destroy)
    end
  end
end
