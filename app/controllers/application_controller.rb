class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :miniprofiler if Rails.env.development? || Rails.env.test?

  private

  def authorize
    redirect_to new_user_session_path unless user_signed_in?
  end

  def miniprofiler
    Rack::MiniProfiler.authorize_request
  end
end
