require "rails_helper"

RSpec.describe Relationship, type: :model do
  it { expect(Relationship.reflect_on_association(:follower).macro).to eq(:belongs_to) }
  it { expect(Relationship.reflect_on_association(:followed).macro).to eq(:belongs_to) }
end
