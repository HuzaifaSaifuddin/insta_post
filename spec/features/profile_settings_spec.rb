require "rails_helper"

RSpec.feature "ProfileSettings", type: :feature do
  load Rails.root.to_s + "/spec/helpers/sessions_helper.rb"

  before :each do
    visit "/users/sign_in"
    @user = (User.all[-1] if User.all[-1].present?) || FactoryBot.create(:user)
    @user2 = (User.all[-2] if User.all[-2].present?) || FactoryBot.create(:user)
    sign_in(@user)

    @user.follow(@user2)
    @user2.follow(@user)
  end

  feature "ProfileSettings" do
    scenario "Should have content", js: true do
      visit "/"
      click_on "Profile"
      sleep 2
      expect(current_path).to eq("/profile_settings")
      expect(page).to have_content(@user.full_name)
      expect(page).to have_content(@user.email)
      expect(page).to have_content("Followers")
      expect(page).to have_content("Following")
      @user.posts.each do |post|
        expect(page).to have_content(post.content)
      end
    end

    scenario "Followers List & Follower Profile", js: true do
      visit "/"
      click_on "Profile"
      sleep 2
      expect(current_path).to eq("/profile_settings")
      click_on @user.followers.count.to_s + " Followers"
      sleep 2
      expect(page).to have_content(@user2.full_name)
      click_on "View Profile"
      sleep 2
      expect(page).to have_content(@user2.full_name)
      expect(page).to have_content(@user2.email)
    end

    scenario "Followings List & Following Profile", js: true do
      visit "/"
      click_on "Profile"
      sleep 2
      expect(current_path).to eq("/profile_settings")
      click_on @user.followers.count.to_s + " Following"
      sleep 2
      expect(page).to have_content(@user2.full_name)
      click_on "View Profile"
      sleep 2
      expect(page).to have_content(@user2.full_name)
      expect(page).to have_content(@user2.email)
    end

    scenario "Follow/Unfollow", js: true do
      visit "/"
      click_on "Profile"
      sleep 2
      expect(current_path).to eq("/profile_settings")
      click_on @user.followers.count.to_s + " Following"
      sleep 2
      expect(page).to have_content(@user2.full_name)
      click_on "Unfollow"
      expect(page).to have_content("Nothing to show")
      click_on "Profile"
      sleep 2
      click_on @user.followers.count.to_s + " Followers"
      sleep 2
      expect(page).to have_content(@user2.full_name)
      click_on "Follow"
      click_on "Profile"
      sleep 2
      expect(page).to have_content("1 Followers")
      expect(page).to have_content("1 Following")
    end
  end
end
