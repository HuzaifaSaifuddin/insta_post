FactoryBot.define do
  factory :post_attachment do
    post_id { 1 }
    avatar { "MyString" }
  end
end
